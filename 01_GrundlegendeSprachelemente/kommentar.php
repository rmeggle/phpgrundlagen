<html>
<head>
<title>Kommentare in PHP</title>
</head>
<body>
<?php
	echo "PHP lernen: "; // einzeiliger Kommentar am Ende eines PHP-Codes
	// einzeiliger Kommentar (komplette Zeile)

	/*
	mehrzeilige Kommentare werden häufig verwendet,
	um den Quellcode ausführlicher zu beschreiben
	*/
	echo "Ich bin dabei!";
?>
</body>
</html>
