<html>
<head>
<title>Funktionen ohne Parameter</title>
</head>
<body>
	<?php
	//Funktionsdefinitionen
	function textausgabe()
		{
		echo "<p>Funktionen erleichtern wiederkehrende Aufgaben.</p>";
		}
	function farbtabelle()
		{
		echo "<table>
			  <tr>
			  <td width='100' style='background-color:#FF0000'>&nbsp;</td>
			  <td width='100' style='background-color:#FFFF00'>&nbsp;</td>
			  <td width='100' style='background-color:#0000FF'>&nbsp;</td>
			  </tr>
			  </table>";		
		}
	//normaler Programmablauf
	textausgabe();
	farbtabelle();
	textausgabe();
	farbtabelle();
	?>
</body>
</html>