<html>
<head>
<title>call-by-reference</title>
</head>
<body>
<?php
	function quadrat($zahl)
		{
		echo "<br>Die Quadratzahl von $zahl ist: ";
		$zahl = $zahl * $zahl;
		echo $zahl;
		}
	$zahl = 2;
	echo '<p>Ausgangswert von $zahl: <b>' .$zahl .'</b></p>';
	echo "<i>call-by-value:</i>";
	for($i = 1; $i <= 5; $i++)		//Schleife: 5-malige Ausführung
		{
		quadrat($zahl);				//call-by-value
		}
	echo "<p><i>call-by-reference:</i>";
	for($i = 1; $i <= 5; $i++)
		{
		quadrat(&$zahl);			//call-by-reference
		}
	?>
</body>
</html>