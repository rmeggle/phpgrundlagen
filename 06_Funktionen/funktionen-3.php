<html>
<head>
<title>Funktionen mit mehreren Parametern</title>
</head>
<body>
	<?php
		function honorar_berechnen($dozent, $stundenzahl, $honorarsatz)
			{
			$honorar = $stundenzahl * $honorarsatz;
			echo "<p>$dozent war $stundenzahl Stunden im Einsatz und hat $honorar Euro verdient.</p>";
			}
		honorar_berechnen("Peter Schmidt", 15, 18);
		honorar_berechnen("Arndt Hoffmann", 38, 15.5);
		honorar_berechnen("Petra Meyer", 27, 23);
	?>
</body>
</html>