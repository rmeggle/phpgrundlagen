<html>
<head>
<title>Funktion mit mehreren R&uuml;ckgabewerten</title>
</head>
<body>
	<?php
		function berechnung($zahl1, $zahl2)
			{
			$quotient = $zahl1 / $zahl2;
			$modulo   = $zahl1 % $zahl2;
			$werte    = array($quotient, $modulo);
			return $werte;
			}
		list($wert1, $wert2) = berechnung(7, 5);
		echo "Quotient: " .$wert1 .", Modulo: " .$wert2;
	?>
</body>
</html>