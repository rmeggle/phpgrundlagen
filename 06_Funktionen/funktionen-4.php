<html>
<head>
<title>Funktion mit R&uuml;ckgabewerten</title>
</head>
<body>
	<?php
		function addiere($zahl1, $zahl2)
			{
			$ergebnis = $zahl1 + $zahl2;
			return $ergebnis;
			}
		addiere(4, 3);	//nur Ausf�hrung der Funktion
		echo "<hr>";
		$summe = addiere(30, 19);	//R�ckgabewert zur Weiterverarbeitung
									//in einer Variablen gespeichert
		echo $summe;
		echo "<hr>";
		echo addiere(17, 4);		//direkte Ausgabe des R�ckgabewertes
	?>
</body>
</html>