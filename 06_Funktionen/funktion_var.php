<html>
<head>
<title>Lokale, globale und superglobale Variablen</title>
</head>
<body>
	<?php
		$bestellnummer = "ABC-12345";
		$bearbeiter = "Maria Schulze";
		function bestellung()
			{
			global $bestellnummer;      // globale Variable
			echo "Bestellnummer: " .$bestellnummer ."<br>";
			echo "Bearbeiter: " .$bearbeiter ."<br>"; //lokale Variable - funktioniert hier nicht!
			echo "Die von Ihnen eingegebene Menge: " .$_POST["menge"] ." kg<br>";
			switch($_POST["sorte"])
				{ 
				case "Jonagold":
				$preis = $_POST["menge"] * 1.50;
				break;
				case "Gala":
				$preis = $_POST["menge"] * 1.65;
				break;
				case "Elstar":
				$preis = $_POST["menge"] * 2.00;
				break;
				}
			echo "Ausgabe des Preises innerhalb der Funktion: $preis<br><br>";
			}
		echo "Die von Ihnen gew�hlte Sorte: " .$_POST["sorte"]."<br><br>";
		bestellung();
		echo "Ausgabe des Preises au�erhalb der Funktion: $preis<br>"; //lokale Variable der Funtkion - funktioniert hier nicht!
	?>
</body>
</html>