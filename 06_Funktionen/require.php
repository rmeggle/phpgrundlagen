<html>
<head>
<title>Dateien einbinden</title>
</head>
<body>
<h3>require: Einbinden eigener Funktionen aus einem anderen Skript</h3>
	<?php
		echo "<p>Ein fremdes PHP-Skript wird eingebunden und die dort enthaltene Funktion ausgef&uuml;hrt:</p>";
		include ("eigene_funktion.inc.php");
		summe(2000, 1376);
		echo "<hr>Hier kann das Skript weitere Anweisungen enthalten.";
	?>
</body>
</html>