<html>
<head>
<title>Funktionen mit optionalen Parametern</title>
</head>
<body>
	<?php
		function berechne($anzahl, $preis = 45, $waehrung = "Euro")
			{
			echo "Ihr Einkauf kostet " .($anzahl * $preis) ." $waehrung.<br>";
			}
		berechne(7, 39.99, "Dollar");	//normaler Aufruf der Funktion
		berechne(10);	//Parameter 2 und 3: Standardwerte werden verwendet
		berechne(15, 29);	//Parameter 3: Standardwert wird verwendet
	?>
</body>
</html>