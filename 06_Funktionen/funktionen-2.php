<html>
<head>
<title>Funktionen mit einem Parameter</title>
</head>
<body>
	<?php
		function quadrat($zahl)
			{
			$ergebnis = $zahl * $zahl;
			echo "<p>Die Quadratzahl von $zahl ist: <b>$ergebnis</b>.</p>";
			}
		function wurzel($zahl)
			{
			$ergebnis = sqrt($zahl);
			echo "<p>Die Wurzel aus $zahl ist: <b>$ergebnis</b>.</p>";
			}
		quadrat(9);
		wurzel(64);
		quadrat(5);
		wurzel(49);
	?>
</body>
</html>