<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


class Person {

	public $Vorname = "";
	public $Nachname = "";

	function __construct() {
	}

}

class Abteilung extends Person {

	public $Abteilungsname = "";

	function __construct() {
		parent::__construct();
	}

}
// Bilden einer Instanz von Person
$p = new Person;

$p->Vorname = "Donald";
$p->Nachname = "Duck";

echo $p->Vorname;
echo $p->Nachname;

echo "<hr>";

// Bilden einer Instanz von Abteilung (Erbt alles aus Person)
$a = new Abteilung;

$a->Vorname = "Dagobert";
$a->Nachname = "Duck";
$a->Abteilungsname = "Panzerschrank";

echo $a->Vorname;
echo $a->Nachname;
echo $a->Abteilungsname;

echo "<hr>";

// static in Klassen: 

class EineKlasse {

	public static $StatischeVariable = '1'; # Static class variable.

}

EineKlasse::$StatischeVariable = "Hexenhaus";
echo EineKlasse::$StatischeVariable;

echo "<hr>";

$c = new EineKlasse;
//echo $c->StatischeVariable; // geht nicht
echo $c::$StatischeVariable; // Ausgabe: Hexenhaus
