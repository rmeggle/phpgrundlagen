<html>
<head>
<title>Schleifenabbruch: continue</title>
</head>
<body>
	<?php 
	$zaehler = 5;		
	for ($nenner = -2; $nenner <= 2; $nenner++)
		{
		if ($nenner == 0)
			{
			echo "Division durch 0 nicht definiert.<br>";
			continue;
			}
		$ergebnis = $zaehler / $nenner;
		echo "$zaehler / $nenner = " .$ergebnis ."<br>";
		}
	?>
</body>
</html>