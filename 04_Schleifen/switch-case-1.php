<html>
<head>
<title>Fallauswahl</title>
</head>
<body>
	<?php
	$gewicht = 36; // Beispiel-Gewicht eines Gep�ckst�cks in kg
	/*
	Gewicht bis 20 kg  = Kategorie S
	Gewicht bis 40 kg  = Kategorie M
	Gewicht bis 60 kg  = Kategorie L
	Gewicht �ber 60 kg = Kategorie XL
	*/
	// es folgt die Berechnung, zu welcher Kategorie 
	// das Gep�ckst�ck geh�rt
	echo "Das Gep&auml;ck wiegt $gewicht kg. Es geh&ouml;rt zur ";
	switch ($gewicht)
		{
		case ($gewicht <= 20):
			echo "Kategorie S (bis 20 kg).";
			break;
		case ($gewicht <= 40):
			echo "Kategorie M (bis 40 kg).";
			break;
		case ($gewicht <= 60):
			echo "Kategorie L (bis 60 kg).";
			break;
		default:
			echo "Kategorie XL (&uuml;ber 60 kg).";
		}
	?>
</body>
</html>
