<html>
<head>
<title>Schleifenabbruch: break</title>
</head>
<body>
	<?php 
	$budget = 50;		// so viel Geld steht zur Verf�gung
	$einzelpreis = 9;	// Einzelpreis des Artikels, der gekauft wird
	$menge = 1;
	while($menge <= 15)	// Idealfall: 15 St�ck kaufen
		{
		$gesamtpreis = $einzelpreis * $menge;
		if ($gesamtpreis > $budget)	//Budget ersch�pft? Wenn ja: Abbruch
			{
			echo "<b>Ihr Budget ist leider ersch&ouml;pft.</b>";
			break;
			}
		echo "$menge St&uuml;ck: $gesamtpreis Euro.<br>";
		$menge++;
		}
	?>
</body>
</html>