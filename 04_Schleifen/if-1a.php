<html>
<head>
<title>Einfache if-Anweisung</title>
</head>
<body>
<h2>Achtung: So funktioniert es nicht (h&auml;ufiger Fehler)</h2>
	<?php
	// ACHTUNG: Dieses Beispiel weist Sie auf einen h�ufigen Fehler hin
		$punkte = 5;
		echo "<p>Variablenwert <b>vor</b> if-Anweisung: " .$punkte ."</p>";
		
		// in der folgenden Bedingungspr�fung soll auf Gleichheit gepr�ft werden
		// Verwenden Sie == (Pr�fung auf inhaltliche Gleichheit) oder === (Gleichheit und gleicher Datentyp)
		// Die Verwendung von = weist einen Wert zu, pr�ft keine Bedingung!
		if ($punkte = 10)
			{
			echo "Die Pr&uuml;fung hat WAHR ergeben.";
			}
		echo "<p>Variablenwert <b>nach</b> if-Anweisung: " .$punkte ."</p>";
			?>
</body>
</html>