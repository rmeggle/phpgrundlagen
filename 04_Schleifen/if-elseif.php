<html>
<head>
<title>if-elseif-else-Anweisung</title>
</head>
<body>
	<?php
		$wochentag = "Freitag";	//testen mit "Mittwoch", "Freitag" und "Samstag"
		echo "Verwendeter Wochentag: $wochentag.<br>";
		if ($wochentag == "Samstag" OR $wochentag == "Sonntag") 
			{
			echo "<br>Es ist Wochenende.<br>";
			}
		elseif ($wochentag == "Freitag") 
			{
			echo "<br>Bald ist Wochenende.<br>";
			}
		else
			{
			echo "<br>Es dauert noch bis zum Wochenende.<br>";
			}
	?>
</body>
</html>
