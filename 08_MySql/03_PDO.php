<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dsn = 'mysql:host=localhost;dbname=course';
$username = 'root';
$password = 'root';
$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
); 

$pdo = new PDO($dsn, $username, $password, $options);

$sql = $pdo->prepare('SELECT Vorname,Nachname FROM tbl_Mitarbeiter WHERE Nachname=:nachname');
$sql->execute(['nachname' => "Duck"]);

echo "<ul>\n";

echo "<table>\n";
echo "<tr>\n";
echo "<th>Vorname</th>";
echo "<th>Nachname</th>";
echo "</tr>\n";
while ($row = $sql->fetch())
{
	echo "<tr>";
	echo "<td>" . $row['Vorname'] . "</td>";
	echo "<td>" . $row['Nachname'] . "</td>";
	echo "</tr>";    
}
echo "</table>\n";
?>
