<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo "Beispiel mit <a href=\"http://adodb.org/dokuwiki/doku.php\">adodb</a><br>";

$host="localhost";
$user="root";
$pass="root";
$database="course";
$driver = "mysqli";

include("./adodb5/adodb.inc.php");

$db = NewADOConnection($driver);
//$db->Connect($host, $user, $pass, $db);
//$db->Connect("localhost", "root", "root", "course");
$db->Connect($host, $user, $pass, $database);

$db->debug=true;

$result = $db->Execute("SELECT * FROM tbl_Mitarbeiter");

if ($result === false) die("failed");

echo "<table>\n";
echo "<tr>\n";
echo "<th>Vorname</th>";
echo "<th>Nachname</th>";
echo "</tr>\n";
while ($arr = $result->FetchRow()) {
	echo "<tr>";
	echo "<td>" . $arr['Vorname'] . "</td>";
	echo "<td>" . $arr['Nachname'] . "</td>";
	echo "</tr>";
}
echo "</table>\n";

// Vorteile, neben der $db->debug bei ado-db sind u.a.:
//$db->BeginTrans(); 
//$ok = $db->Execute("update foo set val=$bar where id=$id"); 
//// ... weitere 1000 Datasets
//$ok = $db->Execute("update foo set val=$bar where id=$id"); 
//$db->CommitTrans($ok); // Auto Rollback wenn einer der DML's fehlgeschlagen ist


