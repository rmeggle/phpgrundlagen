<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo "Beispiel mit mysql(i) native connection</a><br>";

$host="localhost";
$user="root";
$pass="root";
$db="course";

$mysqli = new mysqli($host, $user, $pass, $db);

// Ausführen einer SQL-Anfrage

$sql = 'select * from tbl_Mitarbeiter;';

if (!$result = $mysqli->query($sql)) {
    // Oh no! The query failed. 
    echo "Sorry, the website is experiencing problems.";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
} else {
	// Yepp - we can execute
	echo "Execution of \"${sql}\" was successfull <br>";
}

if ($result->num_rows === 0) {
    // Oh, no rows! Sometimes that's expected and okay, sometimes
    // it is not. You decide. In this case, maybe actor_id was too
    // large? 
    echo "We could not find a match for ID $aid, sorry about that. Please try again.<br>";
    exit;
} else {
	// Oh yes... we do have rows!
	echo "We found " .  $result->num_rows . " rows from the query " . $sql . "<br>";
}

echo "<ul>\n";

echo "<table>\n";
echo "<tr>\n";
echo "<th>Vorname</th>";
echo "<th>Nachname</th>";
echo "</tr>\n";
while ($mitarbeiter = $result->fetch_assoc()) {
	echo "<tr>";
	echo "<td>" . $mitarbeiter['Vorname'] . "</td>";
	echo "<td>" . $mitarbeiter['Nachname'] . "</td>";
	echo "</tr>";
}

echo "</table>\n";
